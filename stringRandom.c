/*
stringRandom.so (c) 2019 Freedom Land Team, GPL v3.0

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
 * */

#include <stddef.h>
#include <stdlib.h>
#include <sys/syscall.h>
#include <unistd.h>
#include "luajit-2.0/lua.h"
#include "luajit-2.0/lualib.h"
#include "luajit-2.0/lauxlib.h"

static int rand_string(lua_State *L)
{
	int size = luaL_checknumber(L, 1);
	
	if (size < 0)
		size *= -1;
	
	char *str = calloc((size_t)size*4, (size_t)size+1);
    const char charset[] = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
    
    int s;
	syscall(SYS_getrandom, &s, sizeof(int), 0);
	srand(s);
    
	for (int n = 0; n < size; ++n) 
	{
		int key = rand() % (int) (sizeof charset - 1);
		str[n] = charset[key];
	}
	
	str[size] = '\0';
	
	lua_pushstring(L, str);
	
	free(str);
    
    return 1;
}

int luaopen_stringRandom(lua_State *L)
{
	lua_getfield(L, LUA_GLOBALSINDEX, "string");
	lua_pushstring(L, "random");
	lua_pushcfunction(L, rand_string);
	lua_settable(L, -3);
	lua_pop(L, 1);
	
	return 0;
}

# string.random

C library to generate random string, from 1 to 40000 characters.

Will only works on Linux!
 
### How to compile

```bash
gcc stringRandom.c -lluajit-5.1 -shared -fPIC -o stringRandom.so
```

Put stringRandom.so in CoreScripts/lib
 
### How to use

You need include library with require("stringRandom"), then you can use it like this:

```lua
print(string.random(10)) -- will print random string with numbers and chars, both upcase and lowcase
```

### License
stringRandom.so (c) 2019 Freedom Land Team, GPL v3.0

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
